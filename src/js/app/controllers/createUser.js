
angular
    .module('userSearch')
    .controller('CreateUserController', 
    ['$scope', 'UserService', 
    function($scope, UserService) {

        $scope.ok = function () {
            UserService
                .save($scope.user, function() {
                    $scope.$close();
                });
        };
          
        $scope.cancel = function () {
            $scope.$dismiss();
        };
    }]);