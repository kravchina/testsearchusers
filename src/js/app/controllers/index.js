
angular
    .module('userSearch')
    .controller('IndexController', 
    ['$scope', '$filter', '$state', 'UserService', '$rootScope',
    function($scope, $filter, $state, UserService, $rootScope) {

        var users = UserService.get(function(users) {
            $scope.users = users;
        });

        $scope.searchChanged = function() {
            $scope.users = $filter('filter')(users, {name: $scope.search});
        };

        $scope.addUser = function() {
            $state.go('index.create');
        };

        $scope.editUser = function(item) {
            $state.go('index.edit', {id: item.id});
        };

        $scope.deleteUser = function(item) {
            UserService.delete(item.id);
        };
    }]);