
angular
    .module('userSearch')
    .controller('EditUserController', 
    ['$scope', '$state',  '$stateParams', 'UserService', 
    function($scope, $state, $stateParams, UserService) {

        UserService.byId($stateParams.id, function(user){
            $scope.user = user;
        });

        $scope.ok = function () {
            UserService
                .update($scope.user, function() {
                    $scope.$close();
                });
        };              
        $scope.cancel = function () {
            $scope.$dismiss();
        };
    }])