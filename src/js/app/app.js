
angular
    .module('userSearch', ['ngResource', 'ui.router', 'ui.bootstrap', 'dndLists'])
    .config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('index', {
                url: '/',
                controller: 'IndexController',
                templateUrl: 'partials/index.html'
            })
            .state('index.create', {
                url: 'create',
                onEnter: ['$stateParams', '$state', '$uibModal',
                    function($stateParams, $state, $modal) {
                        $modal.open({
                            templateUrl: 'partials/user.html',
                            controller: 'CreateUserController'
                        })
                        .result.finally(function() {
                            return $state.transitionTo("index");
                        });
                    }
                ]
            })
            .state('index.edit', {
                url: 'edit/:id',
                onEnter: ['$stateParams', '$state', '$uibModal',
                    function($stateParams, $state, $modal) {
                        $modal.open({
                            templateUrl: 'partials/user.html',
                            controller: 'EditUserController'
                        })
                        .result.finally(function() {
                            return $state.transitionTo("index");
                        });
                    }
                ]
            });
    }]);