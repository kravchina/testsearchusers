
angular
    .module('userSearch')
    .factory('UserService', ['$resource', '$filter', function($resource, $filter) {
        var users = $resource('users.json',{}, {get: {isArray: true}}).get(),
            fakeId = 11;

        return {
            get: function(callback) {
                callback(users);
            },
            byId: function(id, callback) {
                var i, user;

                for (i = 0 ; i < users.length ; i++) {
                    if(users[i].id == id) {
                        user = users[i];
                        break;
                    }
                }

                if (angular.isFunction(callback)) {
                    callback.call(null, user);
                }
            },
            save: function(user, callback) {
                user.id = fakeId++;

                users.push(user);

                if (angular.isFunction(callback)) {
                    callback.call(null, user);
                }
            },
            update: function(user, callback) {
                var i;

                for (i = 0 ; i < users.length ; i++) {
                    if(users[i].id === user.id) {
                        users[i] = user;
                        break;
                    }
                }

                if (angular.isFunction(callback)) {
                    callback.call(null, user);
                }
            },
            delete: function(id, callback) {
                var i;

                for (i = 0 ; i < users.length ; i++) {
                    if(users[i].id === id) {
                        users.splice(i, 1);
                        break;
                    }
                }

                if (angular.isFunction(callback)) {
                    callback.call();
                }
            }
        }
    }]);