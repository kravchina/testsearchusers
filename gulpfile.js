var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gutil = require('gutil'),
    connect = require('gulp-connect');

gulp.task('build-js', function() {
    return gulp.src([
            'src/js/vendor/angular.min.js',
            'src/js/vendor/angular-ui-router.min.js',
            'src/js/vendor/angular-resource.min.js',
            'src/js/vendor/angular-drag-and-drop-lists.min.js',
            'src/js/vendor/ui-bootstrap-custom-tpls-1.0.3.min.js',
            'src/js/app/app.js',
            'src/js/app/controllers/*.js',
            'src/js/app/services/*.js',
            ])
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('app.js'))
        .pipe(uglify())
           .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('build/js'));
});

gulp.task('build-css', function() {
    return gulp.src('src/css/**/*.css')
        .pipe(sourcemaps.init())
        .pipe(concat('style.css'))
        .pipe(minifyCss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('build/css'));
});

gulp.task('build-html', function() {
    return gulp.src('src/partials/**/*.html')
        .pipe(gulp.dest('build/partials/'));
});

gulp.task('copy', function(){
    return gulp.src([
            'src/index.html',
            'src/users.json'
            ])
        .pipe(gulp.dest('build'));
});

gulp.task('build', ['build-js', 'build-css', 'build-html', 'copy']);

gulp.task('server', function() {
  connect.server({
    root: ['build'],
    port: 8008,
    livereload: false
  });
});

gulp.task('watch', function() {
    gulp.watch('src/**/*.*', ['build']);
});

gulp.task('default', ['build', 'watch'], function() {
    gulp.start('server');
});